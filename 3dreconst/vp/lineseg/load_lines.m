function lines = load_lines(fname)

points = load("-ascii", "../geom/lines.tsv");
lines = struct('point1', mat2cell(points(:,1:2),ones(1,size(points,1)),2), ...
               'point2', mat2cell(points(:,3:4),ones(1,size(points,1)),2));


for i = 1:length(lines)
    lines(i).length = norm(lines(i).point1 - lines(i).point2);
end
