import numpy as np

def _line_intersect(line1, line2):
    """ Returns None if lines are parallel """
    x1, y1, x2, y2 = line1
    x3, y3, x4, y4 = line2

    if (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1) == 0:
        # TDOO: check this
    	return None

    return (x1 + (x2-x1) * ((x4-x3)*(y1-y3)-(y4-y3)*(x1-x3))/((y4-y3)*(x2-x1)-(x4-x3)*(y2-y1)),
            y1 + (y2-y1) * ((x4-x3)*(y1-y3)-(y4-y3)*(x1-x3))/((y4-y3)*(x2-x1)-(x4-x3)*(y2-y1)))


def _is_in_image(p, imgwidth, imgheight, MARGIN=0):
    # % at least MARGIN pixels inside the image
    if (1+MARGIN) <= p[0]<=(imgwidth-MARGIN) and (1+MARGIN)<=p[1]<=(imgheight-MARGIN):
        return True
    return False


def _get_junctype(cornerpt, line1, line2, lc1, lc2):
    # junctype: 1--2
    #           |  |
    #           3--4
    # HORZONRIGHT = 1
    # HORZONLEFT = -1
    # HORZONTOP = 1
    # HORZONBOT = -1
    # OTHER = 0

    if lc1==0 and lc2==1:
        hl = line2
        vl = line1
    elif lc1==1 and lc2==0:
        hl = line1
        vl = line2
    elif lc1==0 and lc2==2:
        hl = line2
        vl = line1
    elif lc1==2 and lc2==0:
        hl = line1
        vl = line2
    elif lc1==1 and lc2==2:
        hl = line1
        vl = line2
    elif lc1==2 and lc2==1:
        hl = line2
        vl = line1
    else:
        return None

    # %%
    TOL = 5
    if cornerpt[0]-TOL <  hl[0] and cornerpt[0]-TOL < hl[2]:
        lr = 'left'
    elif cornerpt[0]+TOL >= hl[0] and cornerpt[0]+TOL >= hl[2]:
        lr = 'right'
    else:
        return 0

    if cornerpt[1]-TOL <  vl[1] and cornerpt[1]-TOL < vl[3]:
        ud = 'up'
    elif cornerpt[1]+TOL >= vl[1] and cornerpt[1]+TOL >= vl[3]:
        ud = 'down'
    else:
        return 0

    if lr == 'left':
        if ud == 'up':
            return 1
        if ud == 'down':
            return 3

    if lr == 'right':
        if ud == 'up':
            return 2
        if ud == 'down':
            return 4

    return None


def sample_roomtype1(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    # sample n room hyps
    # 3 ways to create hypothesis
    # recipe1: 1,left 2,bottom, junc2type,3
    # recipe2: 1,left 3,bottom,left, junc2type,4
    # recipe3: 2,bottom 3,bottom,left, junc2type,1

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    oneleft = np.flatnonzero(np.logical_and(lc==0, lr==-1))
    twobottom = np.flatnonzero(np.logical_and(lc==1, tb==-1))
    threebottomleft = np.flatnonzero(np.logical_and(np.logical_and(lc==2, tb==-1), lr==-1))

    linecode = [oneleft, twobottom, threebottomleft]

    # % recipe: [linecode1 linecode2 junc2type]
    recipe = [[0, 1, 3],
              [0, 2, 4],
              [1, 2, 1]]


    check = False
    for i, j, _ in recipe:
        # TODO: remove empty linecode
        check = check or (len(linecode[i]) > 0 and len(linecode[j]) > 0)

    if not check:
        #print('sample_roomtype1 empty')
        return [] # fail

    roomhyp = []
    num_continue_without_progress = 0
    while len(roomhyp) < n and num_continue_without_progress < 100: # TODO: review this
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        l1 = linecode[cur_recipe[0]]
        l2 = linecode[cur_recipe[1]]

        if len(l1)<1 or len(l2)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]
        ls2 = l2[np.random.randint(0, len(l2))]

        cornerpt = _line_intersect(lines[ls1], lines[ls2])

        if cornerpt is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        junctype = _get_junctype(cornerpt,
                                 lines[ls1], lines[ls2],
                                 lines_vp[ls1], lines_vp[ls2])

        if junctype != cur_recipe[2]:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        # check if inside img
        MARGIN = 5
        if not _is_in_image(cornerpt, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        # found a valid way to generate a room hyp
        num_continue_without_progress = 0
        roomhyp.append([None,
                        None,
                        cornerpt, # roomhyp(count).corner(3).pt = cornerpt
                        None, # roomhyp(count).corner(4).pt = [] % up to 4 corners
                        1]) # roomhyp(count).type = 1

        num_continue_without_progress = 0

    return roomhyp


def sample_roomtype2(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    #% sample n room hyps
    #% 3 ways to create hypothesis
    #% recipe1: 1,right 2,bottom, junc2type4
    #% recipe2: 1,right 3,bottom,right, junc2type3
    #% recipe3: 2,bottom 3,bottom,right, junc2type2

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    oneright = np.flatnonzero(np.logical_and(lc==0, lr==1))
    twobottom = np.flatnonzero(np.logical_and(lc==1, tb==-1))
    threebottomright = np.flatnonzero(np.logical_and(np.logical_and(lc==2, tb==-1), lr==1))

    linecode = [oneright, twobottom, threebottomright]

    #% recipe: [linecode1 linecode2 junc2type]
    recipe = [[0, 1, 4],
              [0, 2, 3],
              [1, 2, 2]]

    check = False
    for i, j, _ in recipe:
        # TODO: remove empty linecode
        check = check or (len(linecode[i]) > 0 and len(linecode[j]) > 0)

    if not check:
        #print('sample_roomtype2 empty')
        return [] # fail

    roomhyp = []
    num_continue_without_progress = 0
    while len(roomhyp) < n and num_continue_without_progress < 100:
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        l1 = linecode[cur_recipe[0]]
        l2 = linecode[cur_recipe[1]]

        if len(l1)<1 or len(l2)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]
        ls2 = l2[np.random.randint(0, len(l2))]

        cornerpt = _line_intersect(lines[ls1], lines[ls2])

        if cornerpt is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        junctype = _get_junctype(cornerpt,
                                 lines[ls1], lines[ls2],
                                 lines_vp[ls1], lines_vp[ls2])

        if junctype != cur_recipe[2]:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(cornerpt, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% found a valid way to generate a room hyp
        num_continue_without_progress = 0
        roomhyp.append([None,
                        None,
                        None,
                        cornerpt, # roomhyp(count).corner(4).pt = cornerpt
                        2]) # roomhyp(count).type = 2

    return roomhyp


def sample_roomtype3(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    #% 4 ways to create hypotheses
    #% roomtype1 + 3,top,left
    #% roomtype1 + 2,top
    #% roomtype6 + 3,bot,left
    #% roomtype6 + 2,bot

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    twotop = np.flatnonzero(np.logical_and(lc==1, tb==1))
    twobot = np.flatnonzero(np.logical_and(lc==1, tb==-1))
    threetopleft = np.flatnonzero(np.logical_and(np.logical_and(lc==2, tb==1), lr==-1))
    threebotleft = np.flatnonzero(np.logical_and(np.logical_and(lc==2, tb==-1), lr==-1))

    linecode = [twotop,
                twobot,
                threetopleft,
                threebotleft]

    #% recipe: [roomtypepart linecode oldcornerid newcornerid]
    recipe = [[1, 2, 2, 0],
              [1, 0, 2, 0],
              [6, 3, 0, 2],
              [6, 1, 0, 2]]

    roomhyp = []
    num_continue_without_progress = 0;
    while len(roomhyp) < n and num_continue_without_progress < 30:
        #% pick recipe
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        #% sample partial room
        if cur_recipe[0] == 1:
            roomhyppart = sample_roomtype1(1, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight)
        elif cur_recipe[0] == 6:
            roomhyppart = sample_roomtype6(1, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight)

        if len(roomhyppart) == 0:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% sample lines
        l1 = linecode[cur_recipe[1]]
        if len(l1)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]

        # % new corner
        oldpoint = roomhyppart[0][cur_recipe[2]]
        newpoint = _line_intersect([vp[0][0], vp[0][1], oldpoint[0], oldpoint[1]], lines[ls1])
        if newpoint is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(newpoint, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% TODO: check location of new line sample
        num_continue_without_progress = 0
        roomhyppart[0][cur_recipe[3]] = newpoint
        roomhyppart[0][-1] = 3 # type = 3
        roomhyp.append(roomhyppart[0])

        num_continue_without_progress = 0

    return roomhyp

def sample_roomtype4(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    #% 4 ways to create hypotheses
    #% roomtype2 + 3,top,right
    #% roomtype2 + 2,top
    #% roomtype7 + 3,bot,right
    #% roomtype7 + 2,bot

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    twotop = np.flatnonzero(np.logical_and(lc==2, tb==1))
    twobot = np.flatnonzero(np.logical_and(lc==2, tb==-1))
    threetopright = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==1), lr==1))
    threebotright = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==-1), lr==1))

    linecode = [twotop,
                twobot,
                threetopright,
                threebotright]

    # % recipe: [roomtypepart linecode oldcornerid newcornerid]
    recipe = [[2, 2, 3, 1],
              [2, 0, 3, 1],
              [7, 3, 1, 3],
              [7, 1, 1, 3]]

    roomhyp = []
    num_continue_without_progress = 0;
    while len(roomhyp) < n and num_continue_without_progress < 30:
        #% pick recipe
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        #% sample partial room
        if cur_recipe[0] == 2:
            roomhyppart = sample_roomtype2(1, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight)
        elif cur_recipe[0] == 7:
            roomhyppart = sample_roomtype7(1, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight)

        if len(roomhyppart) == 0:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% sample lines
        l1 = linecode[cur_recipe[1]]
        if len(l1)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]

        # % new corner
        oldpoint = roomhyppart[0][cur_recipe[2]]
        newpoint = _line_intersect([vp[0][0], vp[0][1], oldpoint[0], oldpoint[1]], lines[ls1])
        if newpoint is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(newpoint, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% TODO: check location of new line sample

        num_continue_without_progress = 0
        roomhyppart[0][cur_recipe[3]] = newpoint
        roomhyppart[0][-1] = 4 # type = 3
        roomhyp.append(roomhyppart[0])

        num_continue_without_progress = 0

    return roomhyp


def sample_roomtype5(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    #% 6 ways to create hypotheses
    #% roomtype3 + 1,right
    #% roomtype3 + 3,top,right
    #% roomtype3 + 3,bot,right
    #% roomtype4 + 1,left
    #% roomtype4 + 3,top,left
    #% roomtype4 + 3,bot,left

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    oneright = np.flatnonzero(np.logical_and(lc==0, lr==1))
    oneleft = np.flatnonzero(np.logical_and(lc==0, lr==-1))
    threetopright = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==1), lr==1))
    threebotright = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==-1), lr==1))
    threetopleft = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==1), lr==-1))
    threebotleft = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==-1), lr==-1))

    linecode = [oneright,
                oneleft,
                threetopright,
                threebotright,
                threetopleft,
                threebotleft]

    #% recipe: [roomtypepart linecode oldcornerid newcornerid oldcornerid2 newcornerid2]
    recipe = [[3, 0, 0, 1, 2, 3], #% could be [3 1 3 4 1 2]
              [3, 2, 0, 1, 2, 3],
              [3, 3, 2, 3, 0, 1],
              [4, 1, 1, 0, 3, 2], #% could be [4 2 4 3 2 1]
              [4, 4, 1, 0, 3, 2],
              [4, 5, 3, 2, 1, 0]]

    roomhyp = []
    num_continue_without_progress = 0;
    while len(roomhyp) < n and num_continue_without_progress < 30:
        #% pick recipe
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        # % sample partial room
        if cur_recipe[0] == 3:
            roomhyppart = sample_roomtype3(1, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight)
        elif cur_recipe[0] == 4:
            roomhyppart = sample_roomtype4(1, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight)


        if len(roomhyppart) == 0:
            num_continue_without_progress = num_continue_without_progress + 1
            continue


        #% sample lines
        l1 = linecode[cur_recipe[1]]
        if len(l1)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]

        # % new corner
        oldpoint = roomhyppart[0][cur_recipe[2]]
        newpoint1 = _line_intersect([vp[1][0], vp[1][1], oldpoint[0], oldpoint[1]], lines[ls1])
        if newpoint1 is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(newpoint1, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue


        #% second new corner
        oldpoint2 = roomhyppart[0][cur_recipe[4]]
        newpoint2 = _line_intersect([vp[1][0], vp[1][1], oldpoint2[0], oldpoint[1]],
                                    [newpoint1[0], newpoint1[1], vp[0][0], vp[0][1]])
        if newpoint2 is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(newpoint1, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% add to hypothesis
        num_continue_without_progress = 0
        roomhyppart[0][cur_recipe[3]] = newpoint1
        roomhyppart[0][cur_recipe[5]] = newpoint2
        roomhyppart[0][-1] = 5 # type = 3
        roomhyp.append(roomhyppart[0])

    return roomhyp

def sample_roomtype6(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    #% sample n room hyps
    #% 3 ways to create hypothesis
    #% recipe1: 1,left 2,top, junc2type,1
    #% recipe2: 1,left 3,top,left, junc2type,2
    #% recipe3: 2,top 3,top,left, junc2type,3

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    oneleft = np.flatnonzero(np.logical_and(lc==1, lr==-1))
    twotop = np.flatnonzero(np.logical_and(lc==2, tb==1))
    threetopleft = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==1), lr==-1))

    linecode = [oneleft,
                twotop,
                threetopleft]

    #% recipe: [linecode1 linecode2 junc2type]
    recipe = [[0, 1, 1],
              [0, 2, 2],
              [1, 2, 3]]

    check = False
    for i, j, _ in recipe:
        # TODO: remove empty linecode
        check = check or (len(linecode[i]) > 0 and len(linecode[j]) > 0)

    if not check:
        #print('sample_roomtype6 empty')
        return [] # fail

    roomhyp = []
    num_continue_without_progress = 0
    while len(roomhyp) < n and num_continue_without_progress < 100:
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        l1 = linecode[cur_recipe[0]]
        l2 = linecode[cur_recipe[1]]

        if len(l1)<1 or len(l2)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]
        ls2 = l2[np.random.randint(0, len(l2))]

        cornerpt = _line_intersect(lines[ls1], lines[ls2])

        if cornerpt is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        junctype = _get_junctype(cornerpt,
                                 lines[ls1], lines[ls2],
                                 lines_vp[ls1], lines_vp[ls2])

        if junctype != cur_recipe[2]:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(cornerpt, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        num_continue_without_progress = 0
        #% found a valid way to generate a room hyp
        roomhyp.append([cornerpt,
                       None,
                        None,
                        None,
                        6]) # roomhyp(count).type = 2

    return roomhyp


def sample_roomtype7(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgwidth, imgheight):
    # % sample n room hyps
    # % 3 ways to create hypothesis
    # % recipe1: 1,right 2,top, junc2type,2
    # % recipe2: 1,right 3,top,right, junc2type,1
    # % recipe3: 2,top 3,top,right, junc2type,4

    lc = lines_vp         # -1,0,1,2
    lr = line_leftorright # 1 if right, -1 if left, 0 if neither
    tb = above_horizon    # 1 if above, -1 if below, 0 if neither

    oneright = np.flatnonzero(np.logical_and(lc==1, lr==1))
    twotop = np.flatnonzero(np.logical_and(lc==2, tb==1))
    threetopright = np.flatnonzero(np.logical_and(np.logical_and(lc==3, tb==1), lr==1))

    linecode = [oneright,
                twotop,
                threetopright]

    #% recipe: [linecode1 linecode2 junc2type]
    recipe = [[0, 1, 2],
              [0, 2, 1],
              [1, 2, 4]]

    check = False
    for i, j, _ in recipe:
        # TODO: remove empty linecode
        check = check or (len(linecode[i]) > 0 and len(linecode[j]) > 0)

    if not check:
        #print('sample_roomtype7 empty')
        return [] # fail

    roomhyp = []
    num_continue_without_progress = 0
    while len(roomhyp) < n and num_continue_without_progress < 100:
        use_recipe_num = np.random.randint(0, len(recipe)) # 0, 1 or 2
        cur_recipe = recipe[use_recipe_num]

        l1 = linecode[cur_recipe[0]]
        l2 = linecode[cur_recipe[1]]

        if len(l1)<1 or len(l2)<1:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        ls1 = l1[np.random.randint(0, len(l1))]
        ls2 = l2[np.random.randint(0, len(l2))]

        cornerpt = _line_intersect(lines[ls1], lines[ls2])

        if cornerpt is None:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        junctype = _get_junctype(cornerpt,
                                 lines[ls1], lines[ls2],
                                 lines_vp[ls1], lines_vp[ls2])

        if junctype != cur_recipe[2]:
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        #% check if inside img
        MARGIN = 5
        if not _is_in_image(cornerpt, imgwidth, imgheight, MARGIN):
            num_continue_without_progress = num_continue_without_progress + 1
            continue

        num_continue_without_progress = 0
        #% found a valid way to generate a room hyp
        roomhyp.append([None,
                        cornerpt,
                        None,
                        None,
                        7]) # roomhyp(count).type = 2

    return roomhyp



def extend_to_imageedge(pt, vp, whichside, imgwidth, imgheight):
    # % whichside: 'up','down'(for vp{1}), 'left','right'(for vp{2}), 'away'(for vp{3})

    dir = np.array(pt) - np.array(vp);
    if whichside == 'up':
        if np.sign(dir[1]) == 1:
            dir = -dir
    if whichside == 'down':
        if np.sign(dir[1])==-1:
            dir = -dir
    if whichside == 'left':
        if np.sign(dir[0])==1:
            dir = -dir
    if whichside =='right':
        if np.sign(dir[0])==-1:
            dir = -dir


    alpha = np.zeros(4)
    intp = np.zeros((4,2))
    #% intersection with top
    #% pt(2) + alpha*dir(2) = 1
    #% int1 = pt + alpha*dir
    alpha[0] = (1 - pt[1]) / (1e-32 if dir[1] == 0 else dir[1])
    intp[0] = pt + alpha[0]*dir
    intp[0][1] = 1 # % numerical precision issues....

    #% intersection with bottom
    #% pt(2) + alpha*dir(2) = imgheight
    #% int2 = pt + alpha*dir
    alpha[1] = (imgheight - pt[1]) / (1e-32 if dir[1] == 0 else dir[1])
    intp[1] = pt + alpha[1]*dir
    intp[1][1] = imgheight # % numerical precision issues....

    # % intersection with left
    # % pt(1) + alpha*dir(1) = 1
    # % int3 = pt + alpha*dir
    alpha[2] = (1 - pt[0]) / (1e-32 if dir[0] == 0 else dir[0])
    intp[2] = pt + alpha[2]*dir
    intp[2][0] = 1 # % numerical precision issues....

    #% intersection with right
    #% p1(1) + alpha*dir(1) = imgwidth
    #% int4 = pt + alpha*dir
    alpha[3] = (imgwidth - pt[0]) / (1e-32 if dir[0] == 0 else dir[0])
    intp[3] = pt + alpha[3]*dir
    intp[3][0] = imgwidth; #% numerical precision issues....

    b = np.zeros(4)

    b[0] = alpha[0]>0 and _is_in_image(intp[0], imgwidth, imgheight)
    b[1] = alpha[1]>0 and _is_in_image(intp[1], imgwidth, imgheight)
    b[2] = alpha[2]>0 and _is_in_image(intp[2], imgwidth, imgheight)
    b[3] = alpha[3]>0 and _is_in_image(intp[3], imgwidth, imgheight)

    b = np.flatnonzero(b)
    #% assert(length(b)==1);
    if len(b)==0:
        return []

    if len(b)==1:
        return intp[b[0]]


    return None


def compute_box_from_room(roomhyp, vp, imgwidth, imgheight):
    #% box(1): left wall
    #% box(2): middle wall
    #% box(3): right wall
    #% box(4): ceiling
    #% box(5): floor

    #% image boundary polygon
    # imgpoly = [1, 1, 1, imgheight, imgwidth, imgheight, imgwidth, 1, 1, 1]


    box = []

    for rh in roomhyp:
        rh_type = rh[-1]

        if rh_type==1:
            pt = rh[2]

            boxpoly1 = [[],                                                          # p1
                        extend_to_imageedge(pt, vp[2], 'away', imgwidth, imgheight), # p2
                        extend_to_imageedge(pt, vp[0], 'up', imgwidth, imgheight),   # p3
                        pt,                                                          # p4
                        2]                                                           # orient

            pt = rh[2]

            boxpoly2 = [extend_to_imageedge(pt, vp[0], 'up', imgwidth, imgheight),
                        pt,
                        [],
                        extend_to_imageedge(pt, vp[1], 'right', imgwidth, imgheight),
                        3]

            box.append((boxpoly1, boxpoly2))

        elif rh_type==2:
            pt = rh[3]

            boxpoly1 = [[],
                        extend_to_imageedge(pt, vp[1], 'left', imgwidth, imgheight),
                        extend_to_imageedge(pt, vp[0], 'up', imgwidth, imgheight),
                        pt,
                        3]

            pt = rh[3]

            boxpoly2 = [extend_to_imageedge(pt, vp[0], 'up', imgwidth, imgheight),
                        pt,
                        [],
                        extend_to_imageedge(pt, vp[2], 'away', imgwidth, imgheight),
                        2]

            box.append((boxpoly1, boxpoly2))

        elif rh_type==3:
            boxpoly1 = [extend_to_imageedge(rh[0], vp[2], 'away', imgwidth, imgheight),
                        extend_to_imageedge(rh[2], vp[2], 'away', imgwidth, imgheight),
                        rh[0],
                        rh[2],
                        2]

            boxpoly2 = [rh[0],
                        rh[2],
                        extend_to_imageedge(rh[0], vp[1], 'right', imgwidth, imgheight),
                        extend_to_imageedge(rh[2], vp[1], 'right', imgwidth, imgheight),
                        3]

            box.append((boxpoly1, boxpoly2))
        elif rh_type==4:

            boxpoly1 = [extend_to_imageedge(rh[1], vp[1], 'left', imgwidth, imgheight),
                        extend_to_imageedge(rh[3], vp[1], 'left', imgwidth, imgheight),
                        rh[1],
                        rh[3],
                        3]

            boxpoly2 = [rh[1],
                        rh[3],
                        extend_to_imageedge(rh[1], vp[2], 'away', imgwidth, imgheight),
                        extend_to_imageedge(rh[3], vp[2], 'away', imgwidth, imgheight),
                        3]

            box.append((boxpoly1, boxpoly2))

        elif rh_type==5:

            boxpoly1 = [extend_to_imageedge(rh[0], vp[2], 'away', imgwidth, imgheight),
                        extend_to_imageedge(rh[2], vp[2], 'away', imgwidth, imgheight),
                        rh[0],
                        rh[2],
                        2]

            boxpoly2 = [rh[0],
                        rh[2],
                        rh[1],
                        rh[3],
                        3]

            boxpoly3 = [rh[1],
                        rh[3],
                        extend_to_imageedge(rh[1], vp[2], 'away', imgwidth, imgheight),
                        extend_to_imageedge(rh[3], vp[2], 'away', imgwidth, imgheight),
                        2]

            box.append((boxpoly1, boxpoly2, boxpoly3))

    return box

def drop_roomhyp12_edge(roomhyp, box, imgwidth):
    # % Deletes roomhyp type 1 and 2 where the top of the wall boundary touches
    # % a side of the image.
    # % This is a temporary fix. get_labelimg_frombox.m did not produce correct
    # % results with such case...
    # %
    # % e.g. +----------------+
    # %      |                |
    # %      |               /|  <-- here.. the vertical edge ends on image side
    # %      |              / |
    # %      |--------------\ |
    # %      |               \|
    # %      +----------------+

    MARGIN = 3

    idxtodrop = [0] * len(roomhyp)
    for i in range(len(roomhyp)):
        box_i = box[i]
        if roomhyp[i][-1] == 1:
            if box_i[0][2][0] < 1+MARGIN:
                idxtodrop[i] = 1
        elif roomhyp[i][-1] == 2:
            if box_i[1][0][0] > imgwidth-MARGIN:
                idxtodrop[i] = 1

    roomhyp_drop = [roomhyp[i] for i in range(len(roomhyp)) if idxtodrop[i] == 0]
    box_drop = [box[i] for i in range(len(roomhyp)) if idxtodrop[i] == 0]

    return roomhyp_drop, box_drop

