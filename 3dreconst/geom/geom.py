import cv2
import numpy as np
from collections import defaultdict
from room import sample_roomtype1
from room import sample_roomtype2
from room import sample_roomtype3
from room import sample_roomtype4
from room import sample_roomtype5
from room import compute_box_from_room
from room import drop_roomhyp12_edge

def get_lines(img_gray,
              threshold1=20,
              threshold2=60,
              aperture_size=3,
              hough_threshold=75):

    min_line_length = max(15, int(0.03 * np.sqrt(img_gray.shape[0]**2 + img_gray.shape[1]**2)))
    max_line_gap = max(3, int(0.1 * min_line_length))

    blur = cv2.GaussianBlur(img_gray, (7,7), 0)
    edges = cv2.Canny(blur, threshold1, threshold2, apertureSize=aperture_size)
    lines = np.array([])
    return cv2.HoughLinesP(edges, 1, np.pi/180, hough_threshold, lines, min_line_length, max_line_gap)


def get_angles(lines, px, py):
    angles = []
    PI = np.arctan2(0., -1.) 
    # TODO: rewrite with numpy matrix (vectorize)
    for x1, y1, x2, y2 in lines:
        mid_x, mid_y = 0.5 * (x1 + x2), 0.5 * (y1 + y2)
        v1_x, v1_y = x2 - mid_x, y2 - mid_y
        v2_x, v2_y = px - mid_x, py - mid_y
        v1_norm = np.sqrt(v1_x*v1_x + v1_y*v1_y)
        v2_norm = np.sqrt(v2_x*v2_x + v2_y*v2_y)

        if (v1_norm < 1e-9 or v2_norm < 1e-9):
            angles.append(0.)
            continue

        arg = (v1_x*v2_x + v1_y*v2_y) / v1_norm / v2_norm

        assert(abs(arg) < 1. + 1e-9)

        arg = min(max(-1., arg), 1.)
        theta = 180. / PI * np.arccos(arg)
        if theta > 90:
            theta = 180 - theta
        angles.append(theta)
    return np.array(angles)


def line_belongto_vp(lines, vp_x, vp_y, thr):
    angles = get_angles(lines, vp_x, vp_y)
    return angles <= thr, angles


def line_intersect(line1, line2):
    """ Returns None for parallel lines """
    x1, y1, x2, y2 = line1
    x3, y3, x4, y4 = line2

    dx1, dy1 = (x2 - x1), (y2 - y1)
    dx2, dy2 = (x4 - x3), (y4 - y3)

    if dy2 * dx1 - dx2 * dy1 == 0:
    	return None

    return (x1 + dx1 * (dx2*(y1-y3)-dy2*(x1-x3))/(dy2*dx1-dx2*dy1),
            y1 + dy1 * (dx2*(y1-y3)-dy2*(x1-x3))/(dy2*dx1-dx2*dy1))


def getvote(vp, lines, max_line_len, thr):
    # TODO: vectorize computations
    if len(lines) == 0:
        return 0

    line_length = [np.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) for x1, y1, x2, y2 in lines]

    angles = get_angles(lines, vp[0], vp[1])

    vote = 0
    for length, ang in zip(line_length, angles):
        if ang < thr:
            vote += 0.3 * (1 - ang / thr) + 0.7 * (length / max_line_len)

    return vote


def find_single_vp(lines, max_line_len, thr):
    """ Lines approximately should belong to the same vp """

    maxvp = None
    maxvote = -1

    # excusive 
    for i in range(len(lines) - 1):
        l1 = lines[i]
        for j in range(i + 1, len(lines)):
            l2 = lines[j]

            vp = line_intersect(l1, l2)
            if vp is None:
                continue
                INFDIST = 1e7 # distance to a far away point...
                x1, y1, x2, y2 = l1
                d = (x2 - x1, y2 - y1)
                d = (d[0] / np.sqrt(d[0]*d[0] + d[1]*d[1]), d[1] / np.sqrt(d[0]*d[0] + d[1]*d[1]))
                vp = (x1 + d[0] * INFDIST, y1 + d[1] * INFDIST)

            vote = getvote(vp, lines, max_line_len, thr)
            if vote > maxvote:
                maxvp = vp
                maxvote = vote

    return maxvp


def vp2vl(vp, f, imgsize):
    # % returns the equation for the vanishing line of planes
    # % that are orthogonal to the lines which are
    # % associated with the vanishing point.
    # %
    # % vl [3x1]: vl(1)*x + vl(2)*y + vl(3) = 0
    # % p1, p2: two points on line
    #
    # % principal point (assume at image center)
    cx = imgsize[1] / 2
    cy = imgsize[0] / 2

    # TODO: make workaround for infinit VP

    # % if vp is at infinity...
    # if vp[0] > 1e4 or vp[1] > 1e4:
    #     # %     vl is a line passing through image center.
    #     a = vp[0]
    #     b = vp[1]
    #     c = - a*cx - b*cy
    #     vl = [a, b, c]
    #     p1 = [cx, cy]
    #     p2 = [cx+b, cy-a]
    #     return vl, p1, p2

    # % normalized coord of vp
    nx = vp[0] - cx
    ny = vp[1] - cy

    # % ray
    # % r = [nx ny f]'
    #
    # % ortho plane
    # % p = r / norm(r)
    #
    # % direction of intersecting line
    # % [nx ny f]' x [0 0 1]' = [ny -nx 0]
    #
    # % one point on line
    # % on imaging plane: z0 = f
    # % on ortho plane:   nx*nx0 + ny*ny0 + f*z0 = 0
    if abs(nx) > abs(ny):
        z0 = f
        ny0 = 0
        nx0 = - (ny * ny0 + f * f) / nx
    else:
        z0 = f
        nx0 = 0
        ny0 = - (nx * nx0 + f * f) / ny

    x0 = nx0 + cx
    y0 = ny0 + cy

    # % line eq in 3D
    # % [nx0 ny0 z0]' + lambda * [ny -nx 0]
    #
    # % line eq in 2D
    # % [x0 y0]' + lambda * [ny -nx]
    # % <==>
    # % ax + by + c = 0
    a = nx
    b = ny
    c = - a * x0 - b * y0

    vl = [a, b, c]

    p1 = [x0, y0]
    p2 = [x0 + ny, y0 - nx]

    return vl, p1, p2


def xy2is(xy, f, imgsize):
    # if ~isreal(xy)
    #     is = [xy(1)/j xy(2)/j 0]
    #     is = is / norm(is)
    # else
    #    is = [xy(1)-imgsize(2)/2 xy(2)-imgsize(1)/2 f]
    #    is = is / norm(is)
    _is = [xy[0]-imgsize[1]/2, xy[1] - imgsize[0]/2, f]
    # TODO: ensure that image shape stores correct dimentions
    norm = np.sqrt(_is[0]*_is[0] + _is[1]*_is[1] + _is[2] * _is[2])
    return [e / norm for e in _is]


def vpset_from_vp1_line_f(vp1, line, f, imgsize):

    vl, p1, p2 = vp2vl(vp1, f, imgsize)

    # % intersection of chosen line and vl
    # % [intpt degen] = line_intersect(p1, p2, line.point1, line.point2)
    vp2 = line_intersect((p1[0], p1[1], p2[0], p2[1]), line)

    # % Is3 = cross([vp1(:)-imgsize([2 1])'/2 f], [vp2(:)-imgsize([2 1])'/2 f])
    Is3 = np.cross(xy2is(vp1, f, imgsize), xy2is(vp2, f, imgsize) )
    vp3 = (Is3[0] / Is3[2] * f + imgsize[1]/2, Is3[1] / Is3[2] * f + imgsize[0]/2)
    return vp2, vp3


def find_other_vp(lines_other, vp1, max_line_len, THRES_THETA, imgsize):

    NUMITER = 200  # number of lines to sample
    # FSWEEP = range(50, 1000, 50)  # focal length to test

    # if len(lines_other) > NUMITER:
    #     linesample = randsample(length(lines_other), NUMITER)
    # else
    #     linesample = 1:length(lines_other)
    # end
    #
    # scorearray = zeros(length(linesample), length(FSWEEP))

    maxvp2, maxvp3, maxf = None, None, None
    maxscore = -1

    for i in range(len(lines_other)):
        for f in range(50, 1000, 50): # focal length to test
            line = lines_other[i]

            vp2, vp3 = vpset_from_vp1_line_f(vp1, line, f, imgsize)

            # % line assignment...
            vv2, vv2angle = line_belongto_vp(lines_other, vp2[0], vp2[1], THRES_THETA)
            vv3, vv3angle = line_belongto_vp(lines_other, vp3[0], vp3[1], THRES_THETA)
            vvboth =  np.logical_and(vv2, vv3)
            vvboth2 = np.logical_and(vvboth, np.less(vv2angle, vv3angle))
            vvboth3 = np.logical_and(vvboth, np.greater_equal(vv2angle, vv3angle))
            vvvv2 = np.logical_or(np.logical_and(vv2, np.logical_not(vv3)), vvboth2)
            vvvv3 = np.logical_or(np.logical_and(vv3, np.logical_not(vv2)), vvboth3)

            # % score
            score = getvote(vp2, lines_other[vvvv2], max_line_len, THRES_THETA) + getvote(vp3, lines_other[vvvv3], max_line_len, THRES_THETA)

            if score > maxscore:
                maxscore = score
                maxf = f
                maxvp2 = vp2
                maxvp3 = vp3
    return maxvp2, maxvp3, maxf

def get_vanishing_points(lines, img_size):
    # find three orthogonal vanishing points by
    # 1. findinhg vertical vp.
    # 2. selecting one additional line.
    # 3. pick the best f for the current triplet of vanishing points.
    # 4. go to 2 and pick the best additional line.

    THRES_THETA = 10
    THRES_THETA_GUESS = 20

    # TODO: precompute line length and find maxlinelngth
    line_length = [np.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) for x1, y1, x2, y2 in lines]
    max_line_len = max(line_length)

    guessvp_x, guessvp_y = 0, -1e5
    guess_lines, angle = line_belongto_vp(lines, guessvp_x, guessvp_y, THRES_THETA_GUESS)

    vp1 = find_single_vp(lines[guess_lines], max_line_len, THRES_THETA)

    print('single vp: ', vp1)

    lc1, _ = line_belongto_vp(lines, vp1[0], vp1[1], THRES_THETA)

    vp2, vp3, f = find_other_vp(lines[np.logical_not(lc1)], vp1, max_line_len, THRES_THETA, img_size)

    print('other vp: ', vp2, vp3, f)

    # INFDIST = 1e7 % distance to a far away point...
    # if ~isreal(vp1), vp1 = INFDIST * vp1 / j end
    # if ~isreal(vp2), vp2 = INFDIST * vp2 / j end
    # if ~isreal(vp3), vp3 = INFDIST * vp3 / j end

    # %% order vp
    imgcenter = [img_size[1]/2, img_size[0]/2]
    d2 = [vp2[0]-imgcenter[0], vp2[1] - imgcenter[1]]
    d3 = [vp3[0]-imgcenter[0], vp3[1] - imgcenter[1]]

    if (d2[0]*d2[0] + d2[1]*d2[1]) < (d3[0]*d3[0] + d3[1]*d3[1]):
        temp = vp2
        vp2 = vp3
        vp3 = temp

    return np.array(vp1), np.array(vp2), np.array(vp3)


def get_orientation_map(lines, vps, line_tags, img_size):

    def move_line_towards_vp(p1, p2, vp, amount):
        n1 = np.sqrt((vp[0]-p1[0])**2 + (vp[1]-p1[1])**2)
        n2 = np.sqrt((vp[0]-p2[0])**2 + (vp[1]-p2[1])**2)
        dir1 = ((vp[0] - p1[0]) / n1, (vp[1] - p1[1]) / n1)
        dir2 = ((vp[0] - p2[0]) / n2, (vp[1] - p2[1]) / n2)
        ratio21 = n2 / n1

        if n1 < amount:
            return None

        newp1 = (p1[0] + dir1[0] * amount, p1[1] + dir1[1] * amount)
        newp2 = (p2[0] + dir2[0] * amount * ratio21, p2[1] + dir2[1] * amount * ratio21)

        return newp1[0], newp1[1], newp2[0], newp2[1]


    def extend_line(line, vp, towards_or_away, constrains, imgwidth, imgheight):
        p1 = (line[0], line[1])
        p2 = (line[2], line[3])

        curp1 = p1
        curp2 = p2
        move_amount = 128  # np.sqrt(imgwidth*imgwidth + imgheight*imgheight) / 2
        while move_amount >= 1:
            newline = move_line_towards_vp(curp1, curp2, vp, towards_or_away * move_amount)

            if newline is None:
                move_amount = int(move_amount / 2)
                continue
                # return line, [curp1[0], curp1[1], curp2[0], curp2[1]]

            newp1 = (newline[0], newline[1])
            newp2 = (newline[2], newline[3])

            success = True
            if ((not 1 <= newp1[0] <= imgwidth) or (not 1 <= newp1[1] <= imgheight)
                or (not 1 <= newp2[0] <= imgwidth) or (not 1 <= newp2[1] <= imgheight)):
                success = False
            else: # check constrain intersection
                if towards_or_away == 1:
                    points = [(line[0], line[1]),
                              (line[2], line[3]),
                              (curp2[0], curp2[1]),
                              (curp1[0], curp1[1])]
                else:
                    points = [(curp1[0], curp1[1]),
                              (curp2[0], curp2[1]),
                              (line[2], line[3]),
                              (line[0], line[1])]

                points = np.array(list(map(lambda e: [int(e[0]), int(e[1])], points)))
                poly = np.zeros((imgheight, imgwidth))
                cv2.fillConvexPoly(poly, points, 1)
                success = np.sum(np.logical_and(constrains, poly)) == 0

            if not success:
                move_amount = int(move_amount / 2)
            else:
                curp1 = newp1
                curp2 = newp2

        return line, [curp1[0], curp1[1], curp2[0], curp2[1]]

    def build_constrains(lines, line_tags, size):
        constrains = defaultdict(lambda: np.zeros(size))
        for tag in np.unique(line_tags):
            if tag == -1:
                continue
            for line in lines[line_tags == tag]:
                p1 = (line[0], line[1])
                p2 = (line[2], line[3])
                cv2.line(constrains[tag], p1, p2, 1)
        return constrains


    lineextimg = defaultdict(lambda: np.zeros(img_size))
    constrains = build_constrains(lines, line_tags, img_size)

    for line, tag in zip(lines, line_tags):
        if tag == -1:
            continue
        # print(line, tag)
        for ext in set(range(3)).difference([tag]):
            for target in set(range(3)).difference([tag, ext]):
                _, line2 = extend_line(line, vps[ext], 1, constrains[target], img_size[1], img_size[0])
                points = [(line[0], line[1]), (line[2], line[3]), (line2[2], line2[3]), (line2[0], line2[1])]
                # print(points)
                cv2.fillConvexPoly(lineextimg[(tag, ext, 1)], np.array(list(map(lambda e: [int(e[0]), int(e[1])], points))), 1)

                _, line2 = extend_line(line, vps[ext], -1, constrains[target], img_size[1], img_size[0])
                points = [(line2[0], line2[1]), (line2[2], line2[3]), (line[2], line[3]), (line[0], line[1])]
                # print(points)
                cv2.fillConvexPoly(lineextimg[(tag, ext, -1)], np.array(list(map(lambda e: [int(e[0]), int(e[1])], points))), 1)

    ao23 = np.logical_or(lineextimg[(1,2,1)], lineextimg[(1,2,-1)])
    ao32 = np.logical_or(lineextimg[(2,1,1)], lineextimg[(2,1,-1)])
    ao13 = np.logical_or(lineextimg[(0,2,1)], lineextimg[(0,2,-1)])
    ao31 = np.logical_or(lineextimg[(2,0,1)], lineextimg[(2,0,-1)])
    ao12 = np.logical_or(lineextimg[(0,1,1)], lineextimg[(0,1,-1)])
    ao21 = np.logical_or(lineextimg[(1,0,1)], lineextimg[(1,0,-1)])
    # aa23 = np.logical_and(lineextimg[(1,2,1)], lineextimg[(1,2,-1)])
    # aa32 = np.logical_and(lineextimg[(2,1,1)], lineextimg[(2,1,-1)])
    # aa13 = np.logical_and(lineextimg[(0,2,1)], lineextimg[(0,2,-1)])
    # aa31 = np.logical_and(lineextimg[(2,0,1)], lineextimg[(2,0,-1)])
    # aa12 = np.logical_and(lineextimg[(0,1,1)], lineextimg[(0,1,-1)])
    # aa21 = np.logical_and(lineextimg[(1,0,1)], lineextimg[(1,0,-1)])

    a = (np.logical_and(ao23, ao32),
         np.logical_and(ao13, ao31),
         np.logical_and(ao12, ao21))

    b = (np.logical_and(np.logical_and(a[0], np.logical_not(a[1])), np.logical_not(a[2])),
         np.logical_and(np.logical_not(a[0]), np.logical_and(a[1], np.logical_not(a[2]))),
         np.logical_and(np.logical_not(a[0]), np.logical_and(np.logical_not(a[1]), a[2])))

    return b


def line_equation_from_two_points(p1, p2):
    lineeq = np.array([p2[1]-p1[1], p1[0]-p2[0], -p1[0]*p2[1] + p2[0]*p1[1]])
    norm = np.sqrt(lineeq[0] * lineeq[0] + lineeq[1] * lineeq[1])
    lineeq /= norm
    # normalize sign to be consistent... (0,0) should have a positive distance
    if abs(lineeq[2]) > 1e-10:
        lineeq = lineeq * np.sign(lineeq[2])
    return lineeq


def distance_of_point_to_line(line, x):
    # line: [3x1] (a,b,c)
    # x: [2x1] (x,y)
    # returns d = ax+by+c
    xh = np.append(x, 1)
    d = np.dot(line, xh)
    return d


def is_line_above_horizon(lines, vp):
    horizon = line_equation_from_two_points(vp[1], vp[2])

    if distance_of_point_to_line(horizon, [0, -10000000]) < 0: # if a really high point is not above horizon
        horizon = -horizon # flip sign of horizon

    def _above_horizon(horizon, p1, p2):
        l1 = np.sign(distance_of_point_to_line(horizon, p1))
        l2 = np.sign(distance_of_point_to_line(horizon, p2))
        if l1==1 and l2==1:
            return 1
        if l1==-1 and l2==-1:
            return -1
        return 0

    return np.array([_above_horizon(horizon, line[:2], line[2:]) for line in lines])


def is_line_leftorright(lines, vp):
    # left or right of vp{3}.. vp{3} is the vp close to image center
    # adds lines(i).leftorright:  1 if right, -1 if left, 0 if neither
    # horizon: [3x1] (a,b,c) parameters of the equation of the horizon in 2D where ax+by+c=0

    # %% centerline is the line connecting vp{1} and vp{3}
    centerline = line_equation_from_two_points(vp[0], vp[2])

    if distance_of_point_to_line(centerline, [10000000, 0]) < 0: # % if a really right point is negative
        centerline = -centerline # % flip sign of horizon

    # %% test all lines
    def _leftorright(centerline, p1, p2):
        l1 = np.sign(distance_of_point_to_line(centerline, p1))
        l2 = np.sign(distance_of_point_to_line(centerline, p2))
        if l1 == 1 and l2 == 1:
            return 1
        if l1 == -1 and l2 == -1:
            return -1
        return 0

    return np.array([_leftorright(centerline, line[:2], line[2:]) for line in lines])


def sample_roomhyp(n, lines, lines_vp, above_horizon, line_leftorright, vp, imgsize):
    # % room type:
    # %  1.      2.       3.        4.        5.           6.     7.
    # %                      \           /       \     /     \      /
    # %                       1--     --2         1---2       1-  -2
    # %    |        |         |         |         |   |       |    |
    # %    3--    --4         3--     --4         3---4
    # %   /          \       /           \       /     \
    # %

    roomhyp = []
    roomhyp += sample_roomtype1(n*2/10, lines, lines_vp, above_horizon, line_leftorright, vp, imgsize[1], imgsize[0])
    roomhyp += sample_roomtype2(n*2/10, lines, lines_vp, above_horizon, line_leftorright, vp, imgsize[1], imgsize[0])
    #% roomhyp = [roomhyp sample_roomtype6(n, lines, vp, imgsize(2), imgsize(1))]
    #% roomhyp = [roomhyp sample_roomtype7(n, lines, vp, imgsize(2), imgsize(1))]

    roomhyp += sample_roomtype3(n*2/10, lines, lines_vp, above_horizon, line_leftorright, vp, imgsize[1], imgsize[0])
    roomhyp += sample_roomtype4(n*2/10, lines, lines_vp, above_horizon, line_leftorright, vp, imgsize[1], imgsize[0])

    roomhyp += sample_roomtype5(n*2/10, lines, lines_vp, above_horizon, line_leftorright, vp, imgsize[1], imgsize[0])

    box = compute_box_from_room(roomhyp, vp, imgsize[1], imgsize[0])

    roomhyp_drop, box_drop = drop_roomhyp12_edge(roomhyp, box, imgsize[1])

    return roomhyp_drop, box_drop

def get_box_lines(rh_type, box):
    #% room type:
    #%  1.      2.       3.        4.        5.
    #%                      \           /       \     /
    #%                       1--     --2         1---2
    #%    |        |         |         |         |   |
    #%    3--    --4         3--     --4         3---4
    #%   /          \       /           \       /     \
    #%

    lines = []

    if rh_type==1:
        lines.append((box[0][1], box[0][3]))
        lines.append((box[1][0], box[1][1]))
        lines.append((box[1][1], box[1][3]))
    elif rh_type==2:
        lines.append((box[0][1], box[0][3]))
        lines.append((box[1][0], box[1][1]))
        lines.append((box[1][1], box[1][3]))
    elif rh_type==3:
        lines.append((box[0][0], box[0][2]))
        lines.append((box[0][1], box[0][3]))
        lines.append((box[1][0], box[1][1]))
        lines.append((box[1][0], box[1][2]))
        lines.append((box[1][1], box[1][3]))
    elif rh_type==4:
        lines.append((box[0][0], box[0][2]))
        lines.append((box[0][1], box[0][3]))
        lines.append((box[0][2], box[0][3]))
        lines.append((box[1][0], box[1][2]))
        lines.append((box[1][1], box[1][3]))
    elif rh_type==5:
        lines.append((box[0][0], box[0][2]))
        lines.append((box[0][1], box[0][3]))
        lines.append((box[1][0], box[1][1]))
        lines.append((box[1][0], box[1][2]))
        lines.append((box[1][1], box[1][3]))
        lines.append((box[1][2], box[1][3]))
        lines.append((box[2][0], box[2][2]))
        lines.append((box[2][1], box[2][3]))

    return lines


# def omap_depthorder(omap, vp):
#
#     def omap2region(omap):
#         return reglabel, regori
#
#     def vpscanlines(vp, scandir, omapsize):
#         return None
#
#     def regionpartialorder(reglabel, regori, scanlineendpts):
#         return None
#
#
#     reglabel, regori = omap2region(omap)
#
#     rpo = [None]*3
#
#     omapsize = omap.shape
#     for scandir in range(0, 3):
#         scanlineendpts = vpscanlines(vp, scandir, omapsize)
#         rpo[scandir] = regionpartialorder(reglabel, regori, scanlineendpts)
#
#     return reglabel, regori, rpo
#
#
# def generate_cuboid_from_omap(omap, vp):
#     reglabel, regori, rpo = omap_depthorder(omap, vp)
#     cuboidhyp_omap = omapobj2cuboidhyp(rpo, reglabel, regori, vp)
#     return cuboidhyp_omap

def get_labelimg_frombox(box, roomhyp, imgwidth, imgheight):
    #% orientlabel_img:
    #% 1 for floor and ceiling
    #% 2 for walls facing left/right
    #% 3 for walls facing front/back

    #	1-floor
    #   2-middle wall
    #   3-right wall
    #   4-left wall
    #   5-ceiling
    #   6-object (not supported yet)

    orientlabel_img = np.ones((imgheight, imgwidth))

    BUFF = 0
    for j in range(len(box)):
        mask = np.zeros((imgheight, imgwidth))
        # print('%d %d %d' % (j, len(box), len(box[j])))
        if not box[j]:
            print('WARN: empty boxes')  # TODO: remove this when all hypothesis are implemented
            continue

        # shouldn't it depend on box type?
        if j == 0: #% leftmost box
            points = [
                      [0, 0],
                      [0, imgheight],
                      box[j][1],
                      box[j][3],
                      box[j][2],
                      box[j][0]]
        elif j == (len(box)-1): #% rightmost box
            points = [
                      [imgwidth, imgheight],
                      [imgwidth, 0],
                      box[j][2],
                      box[j][0],
                      box[j][1],
                      box[j][3]]
        else:
            # print('box %s' % box[j])
            points = [box[j][2],
                      box[j][0],
                      box[j][1],
                      box[j][3]]

        # print('poly: %s' % str(points))
        points = [[int(p[0]), int(p[1])] for p in points if len(p) == 2]

        cv2.fillConvexPoly(mask, np.array(points), 1)

        orientlabel_img[mask==1] = box[j][-1]  # orientaition

    return orientlabel_img


def get_3dboxes(img_path, dump_lines = None):
    """ Reconstruct 3d box from image"""
    img = cv2.imread(img_path)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    print('img shape: %dx%d' % img_gray.shape)

    lines = get_lines(img_gray)
    print('len(lines): ', len(lines))

    lines = np.squeeze(lines, axis=1)

    # if dump_lines is not None:
    #     with open(dump_lines, 'w') as dst:
    #         dst.writelines('%s\n' % '\t'.join(map(str, line)) for line in lines)

    def get_longest(lines, fraction=0.25):
        diff = lines[:,2:] - lines[:,:2]
        length = np.sqrt(np.sum(np.power(diff, 2), axis=1))
        indices = sorted([(e, i) for i, e in enumerate(length)], reverse=True)
        top = [i for _, i in indices[:max(1, int(fraction * len(indices)))]]
        return lines[top]

    long_lines = get_longest(lines, 0.99)
    if dump_lines is not None:
        with open(dump_lines, 'w') as dst:
            dst.writelines('%s\n' % '\t'.join(map(str, line)) for line in long_lines)

    vp1, vp2, vp3 = get_vanishing_points(long_lines, img_gray.shape)
    # vp1, vp2, vp3 = np.array([351.2,2210.1]), np.array([1424.7,45.8]), np.array([-113.7,-25.9])
    print('vanishing points: ', vp1, vp2, vp3)

    vp = (vp1, vp2, vp3)

    lines_vp1, _ = line_belongto_vp(lines, vp1[0], vp1[1], 10)
    lines_vp2, _ = line_belongto_vp(lines, vp2[0], vp2[1], 10)
    lines_vp3, _ = line_belongto_vp(lines, vp3[0], vp3[1], 10)

    lines_vp = np.array([np.argmax(e) if sum(e) == 1 else -1 for e in zip(lines_vp1, lines_vp2, lines_vp3)])

    for (is_vp1, is_vp2, is_vp3, (x1, y1, x2, y2)) in zip(lines_vp1, lines_vp2, lines_vp3, lines):
        color = tuple(int(a*b) for a, b in zip([255]*3, (is_vp1, is_vp2, is_vp3)))
        cv2.line(img, (x1, y1), (x2, y2), (color if sum(color) == 255 else (128, 128, 64)), 2)

    cv2.imwrite('houghlines5.jpg', img)

    omap = get_orientation_map(lines, vp, lines_vp, img_gray.shape)

    omap_img = np.zeros(img.shape)
    omap_img[:,:,2] = omap[0] * 255
    omap_img[:,:,1] = omap[1] * 255
    omap_img[:,:,0] = omap[2] * 255

    cv2.imwrite('omap.jpg', img * 0.75 + omap_img * 0.25)

    # cuboidhyp_omap = generate_cuboid_from_omap(omapmore, vp)

    above_horizon = is_line_above_horizon(lines, vp)
    line_leftorright = is_line_leftorright(lines, vp)

    roomhyp, box = sample_roomhyp(1000, lines, lines_vp, above_horizon, line_leftorright, vp, img_gray.shape)
    return roomhyp, box, img_gray.shape, omap


def get_fields(roomhyps, boxes, imgsize, omap, n=None):
    """ Classify pixels
    	1-floor
        2-middle wall
        3-right wall
        4-left wall
        5-ceiling
        6-object (not supported yet)
    """
    if n is None:
        n, score = max(score_roomhyp(roomhyps, boxes, imgsize, omap), key=lambda e: e[1])

    labeling = get_labelimg_frombox(boxes[n], roomhyps[n], imgsize[1], imgsize[0])
    return labeling, boxes[n], roomhyps[n]


def score_roomhyp(roomhyps, boxes, imgsize, omap, weights = None):


    omap_ = np.zeros(shape=imgsize)
    omap_[omap[0]] = 1
    omap_[omap[1]] = 2
    omap_[omap[2]] = 3

    unique_omap = np.unique(omap_)
    weights = weights or dict((label, 1.) for label in unique_omap)

    def _score(labeling):
        result = 0
        for label in [1,2,3]:
           intersection = np.sum(np.logical_and(omap_ == label, omap_ == labeling))
           # unioun = np.sum(np.logical_or(omap_ == label, labeling == label))
           # result += weights.get(label, 1.) * intersection / unioun
           result += weights.get(label, 1.) * intersection / (1e-32 + np.sum(omap_ == label))
        return result

    for i in range(len(roomhyps)):
        # TODO: remap field type and omap types
        labeling = get_labelimg_frombox(boxes[i], roomhyps[i], imgsize[1], imgsize[0])
        labeling[labeling == 4] = 3
        labeling[labeling == 5] = 1
        yield i, _score(labeling)


if __name__ == '__main__':

    np.random.seed(12345)

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--img', default='uiuc261.jpg')
    parser.add_argument('--prefix', default=None)
    parser.add_argument('--dump_lines', default=None)
    parser.add_argument('--n', default=1, type=int)
    parser.add_argument('--seed', type=int, default=12345)

    args = parser.parse_args()

    if args.seed:
        np.random.seed(args.seed)

    img = cv2.imread(args.img)
    roomhyp, boxes, imgsize, omap = get_3dboxes(args.img, args.dump_lines)

    n_roomhyp = np.random.randint(0, len(roomhyp))

    print('len(box): %s' % len(boxes))

    def draw_room_hyp(rh, b, img):
        lines = get_box_lines(rh[-1], b)
        color = np.random.randint(0, 255, 3, dtype='int')
        color = [int(e) for e in color]
        for p1, p2 in lines:
            if len(p1) != 2 or len(p2) != 2:
                print('ERROR: empty points: %s %s %s' % (p1, p2, roomhyp[i]))
                continue
            p1 = (int(p1[0]), int(p1[1]))
            p2 = (int(p2[0]), int(p2[1]))
            cv2.line(img, p1, p2, color, 2)

    # for i in np.random.randint(0, len(roomhyp), 10):
    #     draw_room_hyp(roomhyp[i], boxes[i], img)

    # labeling, box_, roomhyp_ = get_fields(roomhyp, boxes, imgsize, omap, n_roomhyp)
    # draw_room_hyp(roomhyp_, box_, img)

    # print('box: %s' % str(box_))
    # print('roomhyp: %s' % (roomhyp_))
    # print('labeling: %s' % np.unique(labeling))

    # labeling_img = np.zeros(img.shape, dtype=int)

    # print('image: %dx%dx%d' % img.shape)
    # print('labeling: %dx%d' % labeling.shape)
    # print('labeling_img: %dx%dx%d' % labeling_img.shape)

    # labeling_img[:,:,2] = 255 * (labeling==1)  # R
    # labeling_img[:,:,1] = 255 * (labeling==2)  # G
    # labeling_img[:,:,0] = 255 * (labeling==3)  # B

    # cv2.imwrite('labeling.jpg', img * 0.75 + labeling_img * 0.25)


    weights = {1: 4., 2: 1., 3: 1.}

    scores = [(score, i) for i, score in score_roomhyp(roomhyp, boxes, imgsize, omap, weights)]
    scores = sorted(scores, reverse=True)

    prefix = args.prefix
    if prefix is None:
        prefix = args.img.split('/')[-1].rsplit('.', 1)[0]

    def move_corner_to(roomhyp, box, x, y, shape):
        dx, dy = 0, 0
        for i in range(len(roomhyp[:-1])):
            if roomhyp[i] is not None:
                dx = x - roomhyp[i][0]
                dy = y - roomhyp[i][1]
                roomhyp[i] = (x, y)
                break
        for e in box:
            for i in range(len(e[:-1])):
                if len(e[i]) != 0:
                    x = e[i][0] + dx
                    y = e[i][1] + dy
                    e[i] = np.array([x, y])
        return roomhyp, box


    for i in range(min(args.n, len(scores))):
        print('score: %.3f idx: %d' % scores[i])
        labeling, box_, roomhyp_ = get_fields(roomhyp, boxes, imgsize, omap, scores[i][1])
        print('box: ', box_)
        print('roomhyp: ', roomhyp_)

        labeling = get_labelimg_frombox(box_, roomhyp_, img.shape[1], img.shape[0])
        # draw_room_hyp(roomhyp_, box_, img)
        labeling_img = np.zeros(img.shape, dtype=int)
        labeling_img[:,:,2] = 255 * (labeling==1) + 128 * (labeling==4) # R
        labeling_img[:,:,1] = 255 * (labeling==2) + 128 * (labeling==5) # G
        labeling_img[:,:,0] = 255 * (labeling==3) + 128 * (labeling==6) # B
        cv2.imwrite('%s_labeling_%03d.jpg' % (prefix, i), img * 0.75 + labeling_img * 0.25)
