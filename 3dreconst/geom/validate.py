import numpy as np
from scipy.io import loadmat
from geom import get_3dboxes 
from geom import get_fields 


def measure_iou(prediction, ground_truth):
    """ Measure IOU for each class of pixels from ground truth """
    def _get_label_iou(label, equals):
        intersection = np.sum(np.logical_and(ground_truth == label, equals))
        union = np.sum(np.logical_or(ground_truth == label, prediction == label))
        return 1. * intersection / union
    labels = np.unique(ground_truth)
    equals = ground_truth == prediction
    return [(label, _get_label_iou(label, equals)) for label in labels]


def validate_mat(img_path, fields_mat_path):
    """ Get fields prediction for pixels compare with ground truth """
    roomhyp, boxes, imgsize, omap = get_3dboxes(img_path)
    prediction, _, _ = get_fields(roomhyp, boxes, imgsize, omap)
    ground_truth = loadmat(fields_mat_path)
    return measure_iou(prediction, ground_truth['fields'])


if __name__ == '__main__':

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('--img', default='../indoordataset/Images/IMG_3652.jpg')
    parser.add_argument('--mat', default='../indoordataset/GTSpatiallayout/IMG_3652_labels.mat')

    args = parser.parse_args()
    measure = validate_mat(args.img, args.mat)

    print(measure)
    print('average iou: %.3f' % np.mean([iou for _, iou in measure]))
